/*
  MouseRFID
  Reads UNO PICO ID 7 ISO Transponders 
  Reads config file that associates RFIDs with actions such as raising voltage on an Arduino pin
  Looks up action associated with detected RFID and takes that action
  Output (initially to serial monitor, eventually to LCD) the ids, actions, and other diagnostics
  
 By default, the RFID reader looks for FDX-B/HDX Read/Write tags. To be safe (since the type is saved
 to non-volatile memory) we specify this type of tag by sending the "SD2" command in setup(). You can 
 also configure the board to read other tags by sending these commands:

    * SD0 -- EM4100 read only
    * SD1 -- T55xx r/w
    * SD2 -- FDX-B/HDX r/w (default)
    * SD3 -- TIRIS 64bit r/w
    * SD4 -- EM4205/EM4305 r/w

Another useful command is MOF, which returns the operating frequency of the antenna. The frequency
should be 134khz for animal tags. If you add cable between the board and the antenna, the capacitance 
changes and detunes the antenna. The frequency can be adjusted by removing turns from the antenna or 
adding a tuning capacitor, as described in the rfidrw-e-ttl.pdf document.
*/

#include <SD.h>
#include "Wire.h"
#include "LiquidCrystal.h"

const int ACTIONPINLOWBOUNDARY  =  2;    // actions are triggered on arduino pins ACTIONPINLOWBOUNDARY
const int ACTIONPINHIGHBOUNDARY =  6;    // through ACTIONPINHIGHBOUNDARY
const int ACTIONPINHIGHDELAY =  2000;    // How long to keep action pin in high state when triggered

// On the Ethernet Shield, CS is pin 4. Note that even if it's not
// used as the CS pin, the hardware CS pin (10 on most Arduino boards,
// 53 on the Mega) must be left as an output or the SD library
// functions will not work.
const int CHIPSELECT = 4;
const char SETTINGSFILENAME[] = "MouseIDs.txt";     // only 8.3 format file names supported by SD library

// LCD message display variables
long lastLCDToggleTime = 0;
boolean lcdMsgIsOn = false;
const int MESSAGEDISPLAYDELAY = 5000; // Duration of message display on LCD

// Connect LCD via i2c, default address #0 (A0-A2 not jumpered)
LiquidCrystal lcd(0);

// Struct for current mouse/chicken/whathaveyou with an RFID tag
// each time an RFID is read, currentSubject is updated
typedef struct {
  String id;
  String nickname;
  String action;
  boolean hasRecord;
} CurrentSubject;

CurrentSubject currentSubject;  

void initializeCurrentSubject() {
  currentSubject.id = "";
  currentSubject.nickname = "";
  currentSubject.action = "";
  currentSubject.hasRecord = false;
}

// Since the Arduino has limited SRAM for variables, this app failed in mysterious ways when 
// the settings file got large (> 5 entries!). So we leave the data in the file and look it up 
// each time. (This was on an Uno--may not be a problem on a Mega.)
String settingsLineMatchingID(String idString) {
  String settingsFileLine ="";
  
  int idStringLen = idString.length() + 1;
  char idStringChars[idStringLen];
  idString.toCharArray(idStringChars, idStringLen);
  
  File settingsFile = SD.open(SETTINGSFILENAME);
  String aLine;

  Serial.print(F("Searching for ID: "));
  Serial.println(idStringChars);

  if (settingsFile) {
    while (settingsFile.available()) {
      aLine = settingsFile.readStringUntil('\n');
     
      // ignore comment and empty lines
      if (!aLine.startsWith("//") && (aLine.length() > 0)) { // ignore comment and empty lines
        if (aLine.startsWith(idString)) {
          settingsFileLine = aLine;
          break;
        }
      }
    }
  }  else { // if the file isn't open:
    Serial.println(F("error opening settings file"));
  }
  settingsFile.close();
  return settingsFileLine;
}

void setCurrentSubjectForIDString(String idString) {
    initializeCurrentSubject();    
    currentSubject.id = idString;
    
    String matchingRecord = settingsLineMatchingID(idString);    
    int matchingRecLen = matchingRecord.length();
   
    if (matchingRecLen > 0) {
      currentSubject.hasRecord = true;

      // records have comma-separated fields:
      // id, action, nickname    
      int firstCommaIndex = matchingRecord.indexOf(',');
      int secondCommaIndex = matchingRecord.indexOf(',', firstCommaIndex+1);
      
      String action = matchingRecord.substring(firstCommaIndex+1, secondCommaIndex);
      action.trim();
      currentSubject.action = action;

      String nickname = matchingRecord.substring(secondCommaIndex+1);
      nickname.trim();
      currentSubject.nickname = nickname;
    } else {
      Serial.println(F("No record found for this id."));
    }
}

void performActionNum(int actionNum) {
  if ((actionNum >= ACTIONPINLOWBOUNDARY) && ( actionNum <= ACTIONPINHIGHBOUNDARY)) {
    digitalWrite(actionNum, HIGH);   // sets the pin on
    delay(ACTIONPINHIGHDELAY);       // pauses for stated milliseconds -- improvement: use a timer rather than delay--see lcdPrintLines()
    digitalWrite(actionNum, LOW);    // sets the pin off
  } else {
    String actionNumString = String(currentSubject.action);
    lcdPrintLines("Action number: ", actionNumString, "is not valid", "");
  }
}

void setup()   {
  // set up hardware serial port
  Serial.begin(9600);
  
  while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

  // make sure that the default chip select pin is set to output, even if you don't use it:
  pinMode(CHIPSELECT, OUTPUT);
  
  // see if the SD card is present and can be initialized:
  if (!SD.begin(CHIPSELECT)) {
    Serial.println(F("Card failed, or not present"));
    // don't do anything more:
    return;
  } else {
    Serial.println(F("SD Card present."));
  }

  // set up output pins
  for (int i=ACTIONPINLOWBOUNDARY; i<=ACTIONPINHIGHBOUNDARY; i++) {
    pinMode(i, OUTPUT);      
  }
  
  // set up software serial port
  Serial1.begin(9600); 
  Serial1.println("SD0"); // "SD2" ensures that we're using FDX-B/HDX (mouse) type. "SD0" is for chicken tags.
  Serial1.flush();
  
  // ready struct for first subject
  initializeCurrentSubject();

  // set up the LCD's number of columns and rows: 
  lcd.begin(20, 4);
  
  String freeRamString = "  Free RAM: "+ String(freeRam(), DEC);
  lcdPrintLines("Initialized.", "", freeRamString, "");
}

void loop() {
  int actionNum = 0;
  
  if ((millis() - lastLCDToggleTime) > MESSAGEDISPLAYDELAY) {
    resetLCDToggleTime();
    lcd.clear();
    lcd.print("Ready...");
  }
 
  if (Serial1.available()) {  // chars incoming from RFID board
    String line;
   
    while (Serial1.available()) {
      line = Serial1.readStringUntil('\n');
    }
    line.trim(); // remove trailing whitespace
    
//    printString("raw ID: *", line);
//    Serial.write("\n");
        
    if (line.startsWith("OK")) { // ignore the RFID board replies of "OK" when sent commands
      // Serial.write("OK\n");
    } else {
      setCurrentSubjectForIDString(line);
      lcdDisplayCurrentSubjectRecord();
          
      if (currentSubject.action.length() > 0) {
        actionNum = currentSubject.action.toInt();  // returns 0 if can't be converted
        
        if (actionNum > 0) {
          if ((actionNum >= ACTIONPINLOWBOUNDARY) && ( actionNum <= ACTIONPINHIGHBOUNDARY)) {
            performActionNum(actionNum);
          } else {
            lcdPrintLines("Associated action for", currentSubject.id, "outside valid pin values.", "");
            Serial.println(F("Associated action is outside valid pin value."));
          }
        } else {
          lcdPrintLines("Action value isn't valid.", "", "", "");
          Serial.println(F("Action value isn't valid."));
        }
      } else {
        lcdPrintLines("ID: "+ currentSubject.id, "No action associated", "with this id", "");
      }
    }
  }
  
  if (Serial.available()) {  // commands we send to RFID board via serial monitor
    int incomingByte = Serial.read();
    
    Serial1.write(incomingByte);
  }  
}

// Diagnostics

// We only have 2K RAM to play with, and overstepping that fails in unpredictable ways
int freeRam() {
  extern int __heap_start, *__brkval; 
  int v; 
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
}

// utilities
void resetLCDToggleTime() {
  lastLCDToggleTime = millis();
}

void lcdDisplayCurrentSubjectRecord() {
  String hasRecord = currentSubject.hasRecord ? "known" : "unknown" ;
  lcdPrintLines("ID: "+ currentSubject.id, "Name: "+ currentSubject.nickname, "Action: "+ currentSubject.action, "Status: "+hasRecord);
}

void lcdPrintLines(String line1, String line2, String line3, String line4) {
  lcd.clear();     // clears and puts cursor at 0,0
  
  if (line1.length()) {
    lcd.print(line1);
  }
  
  lcd.setCursor(0,1);

  if (line2.length()) {
    lcd.print(line2);
  }
  
  lcd.setCursor(0,2);

  if (line3.length()) {
    lcd.print(line3);
  }
  
  lcd.setCursor(0,3);

  if (line4.length()) {
    lcd.print(line4);
  }
  resetLCDToggleTime();
}

void printString(String prefix, String stringObj) {
  String outputString;  

  if (prefix.length() > 0) {
    outputString = prefix + stringObj;
  } else {
    outputString = stringObj;
  }
 
  int charBufLen = outputString.length() + 1; // add room for trailing null character
  char charBuf[charBufLen];
  outputString.toCharArray(charBuf, charBufLen);

//  Serial.println('\n');
  Serial.print(charBuf);
  Serial.print(F("*"));
  Serial.println(F("\n"));
}


